<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'seances_description' => 'Gestion de dates de séances et de leur répétition. Un spectacle, une projection = un article auquel sont associés des séances. Une séance est définie par une date et un endroit. Les endroits sont définis indépendamment et peuvent être associés à un article.',
	'seances_slogan' => 'Gestion de dates de séances et de leur répétition',
);
?>